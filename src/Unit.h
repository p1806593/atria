//
// Created by constantin on 21/01/2021.
//

#ifndef ATRIA_UNIT_H
#define ATRIA_UNIT_H

#include "Job.h"
#include "Update.h"
#include "Ownable.h"
#include "Position.h"

class Castle;

class Unit : public Job, public Update, public Ownable, public Position{
public:
    Unit(const unsigned int &posX, const unsigned int &posY, const int &_ownerID);
    ~Unit()=default;
    bool bIsAlive;
    Castle * targetCastle;
    void respawn(const Castle &_c);
    void moveUnit();
    void work() override;
    void update() override;
};


#endif //ATRIA_UNIT_H
