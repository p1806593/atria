//
// Created by constantin on 25/01/2021.
//

#include "Renderer.h"
#include "Manager.h"
#include "iostream"
#include "Grapic.h"

void Renderer::draw() {
    grapic::winClear();
    for (auto &tA : Manager::w->tileArray) {
        setColor(tA);
        //grapic::put_pixel(tA.position.x,tA.position.y,r,g,b);
        int scale = Manager::w->getWorldScale();
        grapic::rectangleFill(tA.position.x * scale, tA.position.y * scale, (tA.position.x + 1) * scale,
                              (tA.position.y + 1) * scale);
    }

    for (auto &cA : Manager::w->castleArray) {
        setColor(*cA, true);
        //grapic::put_pixel(tA.position.x,tA.position.y,r,g,b);
        int scale = Manager::w->getWorldScale();
        grapic::rectangleFill((cA->position.x - 2) * scale, (cA->position.y - 2) * scale, (cA->position.x + 3) * scale,
                              (cA->position.y + 3) * scale);
        grapic::print((cA->position.x+5)*scale, (cA->position.y -5)*scale,(int)cA->ownerID);
        grapic::print((cA->position.x+5)*scale, (cA->position.y +1)*scale,(float)cA->Current_soldiers);
    }

    for (auto &uA : Manager::w->unitArray) {
        setColor(*uA, true);
        //grapic::put_pixel(tA.position.x,tA.position.y,r,g,b);
        int scale = Manager::w->getWorldScale();
        if (uA->bIsAlive) {
            grapic::circleFill(uA->position.x*scale, uA->position.y*scale, 4);
        }
    }

    grapic::winDisplay();
}

Renderer::Renderer(const unsigned int & _sizeX, const unsigned int & _sizeY) {
    grapic::winInit("Atria", _sizeX, _sizeY);
    grapic::backgroundColor(120, 120, 120);
    grapic::fontSize(24);
}

void Renderer::setColor(const Ownable & _owner,const bool & bIsUnit) {
    r=_owner.r;
    g=_owner.g;
    b=_owner.b;
    if (!bIsUnit){
        r= ((float)r*0.8f);
        g= ((float)g*0.8f);
        b= ((float)b*0.8f);
    }
    grapic::color(r, g, b);
}

