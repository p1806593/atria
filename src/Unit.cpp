//
// Created by constantin on 21/01/2021.
//

#include "Unit.h"
#include "Castle.h"
#include "Manager.h"
#include "Grapic.h"

Unit::Unit(const unsigned int &posX, const unsigned int &posY, const int &_ownerID) : Position(posX, posY), Ownable(_ownerID){
    bIsAlive=false;
}

void Unit::work() {
    if (bIsAlive) {
        if (targetCastle == nullptr) {
            targetCastle= Manager::w->getClosestEnemyCastle(this, this);
        }
        if (targetCastle != nullptr) {
            moveUnit();
        }
    }
}

void Unit::update() {
    scheduleJob();
}

void Unit::respawn(const Castle & _c) {
    position=_c.position;
    ownerID=_c.ownerID;
    r=_c.r;
    g=_c.g;
    b=_c.b;
    bIsAlive= true;
    targetCastle= nullptr;
}

void Unit::moveUnit() {
    //Vector TargetV(0,0);
    Vector TargetV = targetCastle->position - position;
    //cout<<S.Closest_Enemy_Castle_Pos.x<<" clos "<<S.Closest_Enemy_Castle_Pos.y<<endl;
    //S.Position= S.Position + 0.6*TargetV; // NE MARCHE PAS A FAIRE
    //S.Position= S.Position + NewVector2(2,2);
    int mov=grapic::irand(1,2);
    //position.x++;
    if(TargetV.x==0)
    {
        mov=2;
    }
    else if(TargetV.y==0)
    {
        mov=1;
    }

    if(mov==1 && TargetV.x != 0)
    {
        if(TargetV.x<0)
            position.x= position.x - 1;
        else position.x= position.x + 1;
    }
    else if(TargetV.y!=0)
    {
        if(TargetV.y<0)
            position.y= position.y - 1;
        else position.y= position.y + 1;
    }
    if (TargetV.x== 0 && TargetV.y==0)
    {
        float Capture_rate=grapic::frand(0,10);
        if(Capture_rate<=3)
        {
            targetCastle->capture(this);
        }
        bIsAlive=false;
    }

}
