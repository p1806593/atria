#include <iostream>
#include <zconf.h>
#include "Parallel.h"
#include "testing.h"
#include "Grapic.h"
#include "World.h"
#include "Manager.h"
#include "Renderer.h"
#include <random>
#include <thread>

std::default_random_engine generator;
std::uniform_real_distribution<float> distribution(0, 1);

Parallel *Manager::p = new Parallel();
World *Manager::w;



struct TestJob : Job{
    int test;
    int Salut;
    float truc;
    void work() override{
        //do things
    }
};




int main() {
    int sizeX = 1000;
    int sizeY = 1000;

    Manager m;
    Renderer r(sizeX,sizeY);
    Manager::w = new World(sizeX, sizeY,4);
    Manager::w->generateWorld();



    for (int j = 0;; ++j) {
        r.draw();
        Manager::w->update();
        //Manager::p->waitForJob();
        //std::this_thread::sleep_for(std::chrono::milliseconds(500));
    }


    //grapic::pressSpace();
    grapic::winQuit();
    return 0;
}
