//
// Created by constantin on 21/01/2021.
//

#ifndef ATRIA_VECTOR_H
#define ATRIA_VECTOR_H

#include <cmath>

class Vector {
public:
    Vector(const int & _x=0, const int & _y=0);
    int x,y;
};

inline
float simpleDist(Vector start, Vector pos)
{
    return (float)((start.x-pos.x)*(start.x-pos.x) + (start.y-pos.y)*(start.y-pos.y));
}

inline
float dist(Vector start, Vector pos)
{
    return std::sqrt((start.x-pos.x)*(start.x-pos.x) + (start.y-pos.y)*(start.y-pos.y));
}

inline
const Vector operator-(const Vector v1,const Vector v2)
{
    Vector tmpVec;
    tmpVec.x=v1.x-v2.x;
    tmpVec.y=v1.y-v2.y;
    return tmpVec;
}


#endif //ATRIA_VECTOR_H
