//
// Created by constantin on 21/01/2021.
//

#ifndef ATRIA_UPDATE_H
#define ATRIA_UPDATE_H


class Update {
public:
    ~Update() {}
    virtual void update()=0;
};


#endif //ATRIA_UPDATE_H
