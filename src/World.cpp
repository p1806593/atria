//
// Created by constantin on 21/01/2021.
//

#include "World.h"
#include "Vector.h"
#include "iostream"
#include "Manager.h"
#include <random>
#include <ctime>
std::default_random_engine generatorWorld;
std::uniform_real_distribution<float> distributionWorld(0, 1);


World::World(const unsigned int &sizeX, const unsigned int &sizeY, const unsigned int &scale) {
    gridSizeX=sizeY/scale;
    gridSizeY=sizeY/scale;
    worldScale=scale;
    //unitArray.reserve(1024);
    generatorWorld.seed(std::time(NULL));
    //generateWorld();
}

void World::update() {

    for(auto &t : tileArray){
        t.update();
    }
    Parallel::GetInstance()->waitForJob();
    for(auto &c : castleArray){
        c->update();
    }
    Parallel::GetInstance()->waitForJob();

    for(auto &u : unitArray){
        u->update();
    }
    Parallel::GetInstance()->waitForJob();

    std::cout<<"num to update : "<<unitArray.size()<<std::endl;
}

void World::generateWorld() {
    tileArray.reserve((gridSizeX*gridSizeY));
    std::vector<Position> genArray;
    genArray.reserve((gridSizeX*gridSizeY)/8);
    for (int indexX=0; indexX<gridSizeX;++indexX){
        for (int indexY = 0; indexY <gridSizeY; ++indexY) {
            tileArray.emplace_back(indexX,indexY);
        }
    }

    for (int indexX=0; indexX<gridSizeX/8;++indexX){
        for (int indexY = 0; indexY <gridSizeY/8; ++indexY) {
            genArray.emplace_back(indexX,indexY);
        }
    }

    for (int cIndex = 0; cIndex < 64 ; ++cIndex) {
        int gen = (distributionWorld(generatorWorld)*genArray.size());
        castleArray.push_back(new Castle(genArray[gen].position.x*8,genArray[gen].position.y*8,cIndex));
        genArray.erase(genArray.begin() + gen);
    }


    for(auto & cT : castleArray){
        Unit* sol;
        for (int i = 0; i<8; ++i) {
            sol = new Unit(cT->position.x, cT->position.y, cT->ownerID);
            sol->r=cT->r;
            sol->g=cT->g;
            sol->b=cT->b;
            cT->soldierList.push_back(sol);
            Manager::w->unitArray.push_back(sol);
        }
    }

}

unsigned int World::getWorldScale() const {
    return worldScale;
}

Castle * World::getClosestEnemyCastle(const Position *_p, const Ownable *_o) const{
    float tmpDist,minDist=250000.f;
    Castle * closest = nullptr;
    for(const auto & cT : castleArray){
        if (_o->ownerID!=cT->ownerID){
            tmpDist = dist(_p->position, cT->position);
            if (tmpDist<minDist){
                closest=cT;
                minDist=tmpDist;
            }
        }
    }
    return closest;
}
