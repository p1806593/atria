//
// Created by constantin on 21/01/2021.
//

#ifndef ATRIA_MANAGER_H
#define ATRIA_MANAGER_H

#include "Update.h"
#include "Parallel.h"
#include "World.h"


class Manager : public Update{
public:
    Manager();
    ~Manager();
    static Parallel *p;
    static World *w;
    void update() override;
};




#endif //ATRIA_MANAGER_H
