//
// Created by constantin on 25/01/2021.
//

#ifndef ATRIA_RENDERER_H
#define ATRIA_RENDERER_H

#include "Ownable.h"

class Renderer {
public:
    Renderer(const unsigned int & _sizeX, const unsigned int & _sizeY);
    void draw();

private:
    void setColor(const Ownable & _owner,const bool & bIsUnit = false);

    int r,g,b;
};


#endif //ATRIA_RENDERER_H
