//
// Created by constantin on 21/01/2021.
//

#ifndef ATRIA_CASTLE_H
#define ATRIA_CASTLE_H

#include "Parallel.h"
#include "Unit.h"
#include "Update.h"
#include "Ownable.h"
#include "Position.h"

class Castle : public Job, public Update, public Ownable, public Position {
public:
    Castle(const unsigned int& posX,const unsigned int& posY, const unsigned int & ownerID);
    ~Castle();
    void work() override;
    void update() override;
    void capture(const Ownable * _o);
    std::vector<Unit*> soldierList;
    float influence;
    float influenceGrowth;
    float maxInfluence =64;
    float Current_soldiers=2;
    float Max_soldiers=10;
};


#endif //ATRIA_CASTLE_H
