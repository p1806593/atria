//
// Created by constantin on 21/01/2021.
//

#ifndef ATRIA_OWNABLE_H
#define ATRIA_OWNABLE_H


class Ownable {
public:
    Ownable(int id=0);
    int r,g,b;
    unsigned int ownerID;
};


#endif //ATRIA_OWNABLE_H
