//
// Created by constantin on 21/01/2021.
//

#include "Tile.h"
#include "Manager.h"
#include <iostream>


Tile::~Tile() {

}

Tile::Tile(const unsigned int& posX,const unsigned int& posY) : Position(posX, posY) ,Ownable(0){
    maxInfluence=0;
}

void Tile::work() {
    maxInfluence*=0.9f;
    influenceCheck();
}

void Tile::update() {
    scheduleJob();
    //Manager::p->addJobToQueue(*this);
}

void Tile::influenceCheck() {
    float tmpDist, tmpInflu, appInflu;
    Castle *cRef= nullptr;
    for(auto &ct : Manager::w->castleArray){
        tmpInflu = ct->influence;
        tmpDist = dist(position, ct->position);
        appInflu = tmpInflu - (tmpDist);
        if (appInflu>=maxInfluence){
            ownerID= ct->ownerID;
            r=ct->r;
            g=ct->g;
            b=ct->b;
            maxInfluence=appInflu;
            cRef=ct;
        }
    }
    /*
    if (cRef != nullptr){
        r=cRef->r;
        g=cRef->g;
        b=cRef->b;
    }*/

}
