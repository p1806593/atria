//
// Created by constantin on 21/01/2021.
//

#ifndef ATRIA_TILE_H
#define ATRIA_TILE_H

#include "Parallel.h"
#include "Update.h"
#include "Position.h"
#include "Ownable.h"

class Tile : public Job, public Update , public Position, public Ownable{
public:
    Tile(const unsigned int& posX,const unsigned int& posY);
    ~Tile();
    void influenceCheck();
    void work() override;
    void update() override;

private:
    float maxInfluence=0.f;
};




#endif //ATRIA_TILE_H
