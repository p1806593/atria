//
// Created by constantin on 20/01/2021.
//

#ifndef ATRIA_TESTING_H
#define ATRIA_TESTING_H
#include "Job.h"
#include <cinttypes>

class testing : public Job{
public:
    testing();

    uint32_t id;
    testing(int i);
    void work() override;
    void test();
};


#endif //ATRIA_TESTING_H
