//
// Created by constantin on 21/01/2021.
//

#ifndef ATRIA_WORLD_H
#define ATRIA_WORLD_H

#include "Tile.h"
#include "Castle.h"
#include "Unit.h"
#include <thread>
#include <mutex>
#include <condition_variable>
#include <vector>

class World : public Update {
public:
    World(const unsigned int & sizeX,const unsigned int & sizeY, const unsigned int &scale=4);
    std::vector<Tile> tileArray;
    std::vector<Castle*> castleArray;
    std::vector<Unit*> unitArray;

    [[nodiscard]] unsigned int getWorldScale() const;
    void generateWorld();
    Castle *getClosestEnemyCastle(const Position *_p, const Ownable *_o) const;
    void update() override;
private:
    unsigned int gridSizeX;
    unsigned int gridSizeY;
    unsigned int worldScale;
};


#endif //ATRIA_WORLD_H
