//
// Created by constantin on 21/01/2021.
//

#ifndef ATRIA_POSITION_H
#define ATRIA_POSITION_H

#include "Vector.h"

class Position {
public:
    Position(const unsigned int & posX, const unsigned  int & posY);
    Vector position;
};


#endif //ATRIA_POSITION_H
