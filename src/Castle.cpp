//
// Created by constantin on 21/01/2021.
//

#include "Castle.h"
#include "Manager.h"


Castle::Castle(const unsigned int &posX, const unsigned int &posY, const unsigned int & ownerID) : Position(posX, posY), Ownable(ownerID){
    influence =4;
    influenceGrowth=1;
    //soldierList.reserve(64);
}

Castle::~Castle() {

}

void Castle::work() {
    if (influence<maxInfluence)
        influence += influenceGrowth;

    if(Current_soldiers<Max_soldiers)
    {
        Current_soldiers += (0.01*(Max_soldiers-Current_soldiers))/2;
    }
    if(Current_soldiers>Max_soldiers)
    {
        Current_soldiers -=0.02;
    }


    for(auto &uR: soldierList){
        if (!uR->bIsAlive){
            uR->respawn(*this);
            break;
        }
    }
}

void Castle::update() {
    scheduleJob();
    //Manager::p->addJobToQueue(*this);
}

void Castle::capture(const Ownable * _o) {
    if (Current_soldiers>1 && _o->ownerID!=ownerID){
        Current_soldiers--;
    } else if(_o->ownerID==ownerID){
        //Current_soldiers+=0.5f;
    } else {
        ownerID = _o->ownerID;
        Current_soldiers=3;
        r = _o->r;
        g = _o->g;
        b = _o->b;
        influence = 0;
    }
}
